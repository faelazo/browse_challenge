import React from 'react';

const itemField = ({item}) => {

      return (
            <div className="">
                  <div className="divImageItem">
                        <img className="imageField" src={item.imageUrl} alt=""></img>
                  </div>
                  <h4>{item.title}</h4>
            </div>
      );
};

export default itemField;
