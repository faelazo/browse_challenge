import React from 'react';
import FormLabel from '@material-ui/core/FormLabel';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';

const detailsView = ({item, episodes, handleSelection, handleChange}) => {
      
      return (
            <div className="divDetailsView">
                  <div className="divImage">
                        <img className="imageField" src={item.imageUrl} alt="" ></img>
                  </div>
                  <div className="divData">
                        <form >
                              <h4>Id: {item.id}</h4><br></br>
                              <h3>Type: {item.type}</h3><br></br>
                              <label htmlFor="titleField">Title: </label>
                              <input type="text" name="titleField" value={item.title} onChange={handleChange.bind(this)}/><br></br>
                              <label htmlFor="categories">Categories: </label>
                              <label type="text" name="categories" />{item.categories.toString()}<br></br>
                              <label htmlFor="releaseDate">Released Date: </label>
                              <input type="date" name="releaseDate" value={item.releaseDate.substring(0,10)}  onChange={handleChange.bind(this)}/><br></br>
                              <h1>{item.score}</h1><br></br>
                              <label htmlFor="synopsis">Synopsis: </label><br></br>
                              <textarea name="synopsis" value={item.synopsis}  onChange={handleChange.bind(this)}/>
                        </form>
                  </div>
                  <div className={episodes.length > 0?"divEpisodes":"hidden"}>
                        <FormLabel component="legend">Episodes</FormLabel>
                        <GridList cols={4}>
                              {episodes.map(episode => (
                              <GridListTile key={episode.id} className="rowField" onClick={() => {handleSelection(episode)}}>
                                    <img src={episode.imageUrl} alt={episode.title} />
                                    <GridListTileBar
                                          title={episode.title}
                                    />
                              </GridListTile>
                              ))}
                        </GridList>
                  </div>
            </div>
      );
};

export default detailsView;
