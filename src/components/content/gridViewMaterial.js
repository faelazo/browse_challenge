import React from 'react';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';

const gridViewMaterial = ({handleSelection,  data_fields}) => {
      console.log(handleSelection);
      return (
            <div className="divGridView">
                  <GridList cols={2.5}>
                        {data_fields.map(field => (
                        <GridListTile key={field.id} className="rowField" onClick={() => {handleSelection(field)}}>
                              <img src={field.imageUrl} alt={field.title} />
                              <GridListTileBar
                              title={field.title}
                              />
                        </GridListTile>
                        ))}
                  </GridList>
            </div>
      );
};

export default gridViewMaterial;
