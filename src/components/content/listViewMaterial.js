import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


const listViewMaterial = ({handleSelection,  data_fields}) => {
    return (
        <div className="tableFields">
            <Table className="table">
                <TableHead>
                    <TableRow>
                    <TableCell align="center">ID</TableCell>
                    <TableCell >TITLE</TableCell>
                    <TableCell align="center">TYPE</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data_fields.map(field => (
                    <TableRow key={field.id} className="rowField" onClick={() => {handleSelection(field)}}>
                        <TableCell align="center">{field.id}</TableCell>
                        <TableCell >{field.title}</TableCell>
                        <TableCell align="center">{field.type}</TableCell>
                    </TableRow>
                    ))}
                </TableBody>
            </Table>
        </div>
    );
};

export default listViewMaterial;
