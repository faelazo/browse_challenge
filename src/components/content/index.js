import React, { Component } from 'react';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';

import ListView from "./listViewMaterial";
import GridView from "./gridViewMaterial";
import DetailsView from "./detailsView";

import data from './../../data/data.json';

class Content extends Component{

      constructor(){
            super();
            this.state = {
                  data_fields: data.contents,
                  checkField: false,
                  checkedShow: true,
                  checkedEpisode: true,
                  checkedList: true
            };
      };

      handleSelection = selection => {

            var episodes = [];

            if (selection.type === "show"){
                  episodes = data.contents.filter(item => {return selection.episodes.includes(item.id)});
            }

            this.setState({
                  checkField: true,
                  fieldSelected: selection,
                  episodesField: episodes
            });
      };

      handleModified = event => {
            var data_aux = this.state.data_fields;
            var index = this.state.data_fields.findIndex(item => {return item.id === this.state.fieldSelected.id});
            
            switch (event.target.name){
                  case 'titleField':
                        data_aux[index].title = event.target.value;
                        break;
                  case 'releaseDate':
                        data_aux[index].releaseDate = event.target.value;
                        break;
                  case 'synopsis':
                        data_aux[index].synopsis = event.target.value;
                        break;
                  default:
                        break;
            }
                      
            this.setState({ data_fields: data_aux});
      };

      handleChange = name => event => {
            
            this.setState({ ...this.state, [name]: event.target.checked});
      };

      getSelectedOption = () => {
            if (this.state.checkField){
                  return (<DetailsView item={this.state.fieldSelected} episodes={this.state.episodesField} handleChange={this.handleModified} handleSelection={this.handleSelection}></DetailsView>);
            }else if (this.state.checkedList){
                  return (<ListView data_fields={this.filter()} handleSelection={this.handleSelection} ></ListView>);
            }else{
                  return (<GridView data_fields={this.filter()} handleSelection={this.handleSelection} ></GridView>);
            }
      };

      filter = () => {
            var listFiltered = [];
            this.state.checkedShow?listFiltered = this.state.data_fields:listFiltered = this.state.data_fields.filter(item => {return item.type !== "show"});
            
            if (!this.state.checkedEpisode) {listFiltered = listFiltered.filter(item => {return item.type !== "episode"});};
            
            return listFiltered;
      }

      render(){
            return (
                  <article id="divContent">
                        <Breadcrumbs separator="›" aria-label="Breadcrumb" className="divBreadcrumb">
                              <label id="labelBread" color="inherit" onClick={()=>{this.setState({checkField: false, fieldSelected: []})}} >
                                    Contents
                              </label>
                              <label color="inherit" >
                                    {this.state.fieldSelected == null?"":this.state.fieldSelected.title}
                              </label>
                        </Breadcrumbs>
                                    
                        <div className={this.state.checkField?"hidden":"divFilter"} >
                              <FormControlLabel
                                    control={
                                          <Checkbox   checked={this.state.checkedShow} 
                                                      onChange={this.handleChange('checkedShow')} 
                                                      value="Show" 
                                                      color="primary"/>
                                    }
                                    label="Show"
                              />
                              <FormControlLabel
                                    control={
                                          <Checkbox   checked={this.state.checkedEpisode} 
                                                      onChange={this.handleChange('checkedEpisode')} 
                                                      value="Show"
                                                      color="primary" />
                                    }
                                    label="Episode"
                              />
                        </div>
                        
                        <div className={this.state.checkField?"hidden":"divTypeView"}>
                              <Grid component="label" container alignItems="center" spacing={1}>
                                    <Grid item>Grid</Grid>
                                    <Grid item>
                                          <Switch
                                                checked={this.state.checkedList}
                                                onChange={this.handleChange('checkedList')}
                                                value="checkedList"
                                          />
                                    </Grid>
                                    <Grid item>List</Grid>
                              </Grid>
                        </div>
                  
                        {this.getSelectedOption()}
                  </article>
            );
      };
};

export default Content;